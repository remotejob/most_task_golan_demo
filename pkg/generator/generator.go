package generator

import (
	"os"
	"strconv"
	"sync"

	"gitlab.com/remotejob/most_task_golan/alog"
)

func Start(wg *sync.WaitGroup, flow int64, num int64) {

	defer wg.Done()

	var n int64 = 0

	for n < num {

		in := "flow " + strconv.FormatInt(flow, 10) + " num " + strconv.FormatInt(n, 10) + "\n"

		os.Stdout.WriteString(alog.Info(in))

		n++

	}

}
