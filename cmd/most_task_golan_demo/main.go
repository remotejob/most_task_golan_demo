package main

import (
	"log"
	"os"
	"strconv"
	"sync"

	"gitlab.com/remotejob/most_task_golan_demo/pkg/generator"
)

var (
	flow int64
	num  int64
	err  error
)

func init() {
	flow, err = strconv.ParseInt(os.Args[1], 10, 64)
	if err == nil {

	}
	num, err = strconv.ParseInt(os.Args[2], 10, 64)
	if err == nil {

	}

}

func main() {

	wg := new(sync.WaitGroup)

	n := int64(1)

	for n <= flow {

		go generator.Start(wg, n, num)
		wg.Add(1)

		n++
	}

	wg.Wait()

	log.Println("flow", flow, "num", num)

}
